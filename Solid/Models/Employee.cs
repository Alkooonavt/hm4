﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solid.Models
{
    public class Employee : IReportable
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        
    }

    public abstract class Employer
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }

    public interface IApply
    {
        void Apply();
    }

    public interface IPaySalary
    {
        decimal PaySalary();
    }

    public class Microsoft : Employer, IApply, IPaySalary
    {
        public void Apply()
        {
            Console.Write("Please call to HR Manager!");
        }

        public decimal PaySalary()
        {
            return 120_000;
        }
    }

    public class Apple : Employer, IPaySalary
    {
        public decimal PaySalary()
        {
            return 140_000;
        }
    }

    public class MyProgram
    {
        public MyProgram()
        {
            var list = new List<Employer>();
            list.Add(new Microsoft());
            list.Add(new Apple());

            foreach (var employer in list)
            {
                employer.FullName = "Microsoft";
            }

            Employer employer1 = new Apple();

        }
    }

}