﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using WebApplication5.Data;
using WebApplication5.Models;
using WebApplication5.ViewModels;

namespace WebApplication5.Controllers
{
    public class TeachersController : Controller
    {
        private ApplicationDataContext db = new ApplicationDataContext();

        // GET: Teachers
        public ActionResult Index()
        {
            var teachers = db.Teachers.Include(c => c.Subject).ToList();
            var tvm = new List<TeacherViewModel>();
            foreach (var teacher in teachers)
            {
                var subjectName = "Not set";
                if (teacher.Subject != null && string.IsNullOrEmpty(teacher.Subject.Title))
                {
                    subjectName = teacher.Subject.Title;
                }

                tvm.Add(new TeacherViewModel()
                {
                    Id = teacher.Id,
                    Name = teacher.Name,
                    SubjectName = subjectName
                });
            }

            var model = new IndexViewModel
            {
                Teachers = tvm,
                CreateTeacherViewModel = new CreateTeacherViewModel()
                {
                    Subjects = db.Subjects.ToList()
                }
            };

            return View(model);
        }

        // GET: Teachers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher teacher = db.Teachers.Find(id);

            if (teacher == null)
            {
                return HttpNotFound();
            }
            return View(teacher);
        }

        // GET: Teachers/Create
        public ActionResult Create()
        {
            ViewBag.Subjects = db.Subjects.ToList();
            return View();
        }

        // POST: Teachers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateTeacherViewModel createTeacherViewModel)
        {
            if (ModelState.IsValid)
            {
                Subject subject = db.Subjects.Find(createTeacherViewModel.SubjectId);
                Teacher teacher = new Teacher()
                {
                    Name = createTeacherViewModel.Name,
                    Subject = subject
                };

                db.Teachers.Add(teacher);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(createTeacherViewModel);
        }

        // GET: Teachers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher teacher = db.Teachers.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            return View(teacher);
        }

        // POST: Teachers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Teacher teacher)
        {
            if (ModelState.IsValid)
            {
                db.Entry(teacher).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(teacher);
        }

        // GET: Teachers/Delete/5
        public ActionResult Delete(int? id,bool confirm_value)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Teacher teacher = db.Teachers.Find(id);
            if (teacher == null)
                return HttpNotFound();

            if (confirm_value == true)
            {
                db.Teachers.Remove(teacher);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }


    
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
